const net = require('net');
const { readFile } = require('fs');
const client = new net.Socket();

const query = "1 CONNECT 2";

const id = 1489;

readFile("../atividade_final/lista_dispositivos.txt", (err, data) => {
    const devices = data.toString().split("\n").map((aux) => {
        const obj = aux.split(' ');
        return {
            name: obj[0],
            ip: obj[1],
            port: obj[2]
        }
    }
    );

    let [origin, command, destination] = query.split(' ');
    // console.log(`Origin: ${origin}, ${command}, ${destination}`);

    //
    let centralDevice = devices[origin - 1];
    let conteinerDevice = devices[destination - 1];

    client.connect(centralDevice.port, centralDevice.ip, async () => {
        const msg = 'LIVRE ' + id + '\n';
        client.write(msg);
        console.log("Wrote " + msg + ' to ' + centralDevice.name);
    });

    client.on('data', (data) => {
        // console.log('Data: ', data);

        const newData = data.toString();
        console.log("Data: ", newData);
        if (newData.includes('RON')) {
            return;
        }

        if (newData.includes('COLETAR')) {
            sendCheguei(centralDevice);
            // client.destroy();
        } else
            return;

    });

    client.on('close', () => {
        console.log('Connection closed');
    })

});

sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    })
}

async function sendCheguei(centralDevice) {
    const msg1 = 'CHEGUEI_CONTAINER ' + id + '\n';
    client.write(msg1);
    console.log("Wrote " + msg1 + ' to ' + centralDevice.name);
    let x = Math.random() * 20000;
    if (x < 5000)
        x = 5000;
    console.log('Waiting ' + (x / 1000) + ' seconds');

    await sleep(x);

    const msg2 = `COLETA_FINALIZADA ${id}\n`;
    client.write(msg2);
    console.log(`Enviado coleta finalizada`);

}
